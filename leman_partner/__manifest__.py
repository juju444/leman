{
    'name': 'Leman Partner',
    'category': 'Extra Tools',
    'summary': 'Add fields on partner',
    'version': '12.0.1.1.1',
    'license': 'AGPL-3',
    'author': 'Open-net',
    'website': 'https://monnaie-leman.org',
    'depends': [
        'contacts',
        'web_view_searchpanel',
        # depends for the php admin interface xmlrpc export
        'partner_contact_gender',
        'partner_contact_nationality',
        'partner_contact_birthdate',
        'partner_industry_secondary',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner.xml',
    ],
    'installable': True,
}
