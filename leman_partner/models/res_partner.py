from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    # Leman specific fields, some might be redondant with lcc_members, check with Lokavaluto
    change_desk = fields.Boolean(
        string="Bureau de change",
    )
    electronic_account = fields.Boolean(
        string="Compte électronique",
    )
    wallet_accounts = fields.Char(
        string="Code et comptes",
    )
    employees_number = fields.Float(
        string="Nombre ETP",
        help="Nombre d'équivalents temps plein (ETP)",
    )
    legal_name = fields.Char(
        string="Nom légal de l'entreprise",
    )
    public_name = fields.Char(
        string="Nom public",
    )
    creation_date = fields.Date(
        string="Date de création",
        help="Date de création de l'entreprise",
    )
    legal_status = fields.Many2one(
        name="Statuts juridiques",
        comodel_name='res.partner.legal.status',
    )
    pep = fields.Boolean(
        string="PEP",
        help="ADE est/sont PEP?",
    )
    pepr = fields.Boolean(
        string="PEP Related",
        help="ADE lié-e-s à une/des PEP?",
    )
    leman_contact_type = fields.Selection(
        [('ade', 'Ayant droit économique'),
         ('ts', 'Tiers'),
         ('postal', 'Adresse postale'),
         ('contact', 'Contact'),
         ],
        string="Type de contact",
        help="Spécifique au léman, utilisé pour trier, aucun impact fonctionnel",
    )
    categ_lemanex = fields.Many2one(
        string="Limites LMX",
        comodel_name="res.partner.limit.lemanex",
    )
    leman_inscription_id = fields.Integer(
        name="ID inscription",
        help="ID dans l'interface admin des inscriptions",
    )

    def migrate_old_fields(self):
        self.ensure_one()
        self.change_desk = self.x_bdc
        self.electronic_account = self.x_compte_electronique_actif
        self.wallet_accounts = self.x_correspndance_clef_publique
        self.employees_number = self.x_employees_number
        self.legal_name = self.x_legal_name
        self.public_name = self.x_public_name
        if self.x_categorie_limit_lemanex_id:
            new_cat = self.env['res.partner.limit.lemanex'].search(
                [('name', '=', self.x_categorie_limit_lemanex_id.x_name)], limit=1)
            if not new_cat:
                new_cat = self.env['res.partner.limit.lemanex'].create(
                    {'name': self.x_categorie_limit_lemanex_id.x_name})
            self.categ_lemanex = new_cat.id
        if self.x_status_juridique_id:
            new_status = self.env['res.partner.legal.status'].search(
                [('name', '=', self.x_status_juridique_id.x_name)], limit=1)
            if not new_status:
                new_status = self.env['res.partner.legal.status'].create(
                    {'name': self.x_status_juridique_id.x_name})
            self.legal_status = new_status.id


class StatutJuridique(models.Model):
    _name = 'res.partner.legal.status'
    _description = "Statut Juridique"

    name = fields.Char(
        string="Nom",
    )


class LimitLemanex(models.Model):
    _name = 'res.partner.limit.lemanex'
    _description = "Limit Lemanex"

    name = fields.Char(
        string="Nom",
    )