# Copyright 2021 Open-Net - Julien Guenat

{
    "name": "Switzerland - MIS reports Leman",
    "summary": "Specific MIS reports for switzerland localization",
    "version": "12.0",
    "license": "AGPL-3",
    "author": "Open-Net Sàrl",
    "website": "www.open-net.ch",
    "depends": ["mis_builder"],
    "data": [
        "data/mis.report.csv",
        "data/mis.report.style.csv",
        "data/mis.report.kpi.csv",
    ],
    "installable": True,
}
